/**hc_sr4.h
 *
 * @section genDesc General Description
 *
 * * Documento header del driver del medidor de distancia.
 * Este driver provee funciones para disparar el medidor de distancia por ultrasonido y recibir la información que este provee.
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/04/2021 | Document creation		                         |
 * | 06/05/2021	| Document completion   						 |
 *
 * @author Flavio Galeano
 *
 */
 /*
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210506 v1.0.0 initial version
 */

#ifndef HC_SR4_H
#define HC_SR4_H

/*==================[inclusions]=============================================*/

#include <stdint.h>
#include "bool.h"
#include "gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[Typedef]==============================================*/

/*Variables para almacenar los puertos de entrada y salida*/
typedef struct {				/*!< Struct de dispositivo */
	gpio_t puerto_entrada;			/*Puerto de entrada*/
	gpio_t puerto_salida;			/*Puerto de salida*/
} us_config;

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/**
 * @fn HcSr04Init
 * @brief Inicialización de puertos
 * @param[in] puertos de entrada y salida
 * @return Bool
 */
bool HcSr04Init(us_config device);



/**
 * @fn HcSr04Init
 * @brief Lectura en centimetros
 * @param[in] void
 * @return Distancia en centimetros
 */

int16_t HcSr04ReadDistanceCentimeters(us_config device);


/**
 * @brief Lectura en centimetros
 * @param[in] void
 * @return Distancia en milimetros
 */
float HcSr04ReadDistanceMilimeters(us_config device);


/**
 * @fn HcSr04Init
 * @brief Lectura en pulgadas
 * @param[in] void
 * @return Distancia en pulgadas
 */
int16_t HcSr04ReadDistanceInches(us_config device);



/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */
