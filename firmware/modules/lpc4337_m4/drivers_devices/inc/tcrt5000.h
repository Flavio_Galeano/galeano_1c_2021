/*! @mainpage Device TRCRT5000
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   TRCT5000		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	DOUT	 	| 	T_COL0		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND	    	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 16/04/2021 | Creación del documento		                 |
 * | 16/04/2021	| Se añaden 3 funciones		                     |
 * | 			| 	                     						 |
 *
 * @author Nicolas Bernabei
 *
 */

#ifndef _TCRT5000_H
#define _TCRT5000_H

/*==================[macros]=================================================*/


/*==================[inclusions]=============================================*/

#include "bool.h"
#include "gpio.h"
#include "led.h"
#include <stdint.h>

/*==================[external functions declaration]=========================*/

/** @fn Tcrt5000Init
 *
 * @brief Funcion que inicializa el dispositivo sensor infrarrojo TCRT5000.
 *
 * @param[in] dout: puerto que inicializa
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */
bool Tcrt5000Init(gpio_t dout);

/** @fn Tcrt5000State
 *
 * @brief Funcion que informa el estado de salida del dispositivo TCRT5000.
 *
 * @param no recibe parámetros
 *
 * @return FALSE si se detecta un obstáculo, en otro caso retorna TRUE
 */
bool Tcrt5000State(void);

/** @fn Tcrt5000Lectura
 *
 * @brief Función que sirve para controlar una correcta conexión del dispositivo TCRT5000.
 * En caso de una correcta conexión se enciende el LED 2 de la EDU-CIAA.
 *
 * @param No recibe parámetros
 *
 * @return No retorna ningún valor
 */
void Tcrt5000Lectura(void);

/** @fn Tcrt5000Cuenta
 *
 * @brief Registra el paso de un objeto, sucediendo un evento de flanco descendente
 * y otro evento de flanco ascendente se ratifica el conteo .
 *
 * @param No recibe parámetros
 *
 * @return FALSE si no ocurre un conteo, en caso de que se produzca el conteo retorna TRUE
 */
uint8_t Tcrt5000Contador(void);

/** @fn Tcrt5000DeInit
 *
 * @brief Funcion que desinicializa el dispositivo TCRT5000. No se encuentra implementada.
 *
 * @param[in] dout: puerto que desinicializa
 *
 * @return FALSE if an error occurs, in other case returns TRUE
 */
bool Tcrt5000Deinit(gpio_t dout);

/*==================[end of file]============================================*/


#endif /* #ifndef _TCRT5000_H */

