/**hc_sr4.c
 *
 * @section genDesc General Description
 *
 * Documento source del driver del medidor de distancia.
 * Este driver provee funciones para disparar el medidor de distancia por ultrasonido y recibir la información que este provee.
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 15/04/2021 | Document creation		                         |
 * | 06/05/2021	| Document completion   						 |
 *
 * @author Flavio Galeano
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210506 v1.0.0 initial version
 */

/*==================[inclusions]=============================================*/
#include "delay.h"
#include "gpio.h"
#include "hc_sr4.h"
#include <stdint.h>
#include "led.h"
#include "systemclock.h"

/*==================[macros and definitions]=================================*/
/*Escalas de conversión tiempo de pulso-distancia*/
#define ESCALA_CM 28
#define ESCALA_INCH 148


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
/**
 * @fn DispararUltrasonido
 * @brief Disparador del medidor
 * @param[in] void
 * @return Bool
 */
void DispararUltrasonido(gpio_t puerto_salida)
{
		/*El puerto de salida se pone en alto por 10us para activar el medidor*/
		GPIOOn(puerto_salida);
		DelayUs(10);
		GPIOOff(puerto_salida);
}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

/**
 * @brief Inicialización de puertos
 * @param[in] puertos de entrada y salida
 * @return Bool
 */
bool HcSr04Init(us_config device)
{
	/*Se inicializan los puertos*/
	GPIOInit(device.puerto_salida, GPIO_OUTPUT);
	GPIOInit(device.puerto_entrada, GPIO_INPUT);

	/*Se asignan los puertos de entrada y salida pasados como parámetro
	 * a las variables correspondientes
	puerto_entrada = echo;
	puerto_salida = trigger;*/

	return true;
}


/**
 * @brief Lectura en centimetros
 * @param[in] void
 * @return Distancia en centimetros
 */
int16_t HcSr04ReadDistanceCentimeters(us_config device)
{
	int16_t contador = 0; /*Contador para la cantidad de us*/
	int16_t medicion = 0; /*Valor calculado de la medición*/

	DispararUltrasonido(device.puerto_salida); /*Se genera el pulso que activa el lector*/

	/*Espera a que se genere el pulso de la señal recibida*/
	while(GPIORead(device.puerto_entrada)==false)
	{

	}
	/*Una vez generado el pulso entra incrementa el valor del contador
	 * en 1 cada us*/
	while(GPIORead(device.puerto_entrada))
	{
		DelayUs(1);
		contador = contador+1;
	}

	/*La medición es proporcional al valor alcanzado por el contador según ESCALA_CM*/
	medicion = contador / ESCALA_CM;

	return medicion;
}


/**
 * @brief Lectura en centimetros
 * @param[in] void
 * @return Distancia en milimetros
 */
float HcSr04ReadDistanceMilimeters(us_config device)
{
	int16_t contador = 0; /*Contador para la cantidad de us*/
	float medicion = 0; /*Valor calculado de la medición*/

	DispararUltrasonido(device.puerto_salida); /*Se genera el pulso que activa el lector*/

	/*Espera a que se genere el pulso de la señal recibida*/
	while(GPIORead(device.puerto_entrada)==false)
	{

	}
	/*Una vez generado el pulso entra incrementa el valor del contador
	 * en 1 cada us*/
	while(GPIORead(device.puerto_entrada))
	{
		DelayUs(1);
		contador = contador+1;
	}

	/*La medición es proporcional al valor alcanzado por el contador según ESCALA_CM*/
	medicion = 10*contador / ESCALA_CM;

	return medicion;
}


/**
 * @brief Lectura en pulgadas
 * @param[in] Void
 * @return Distancia en pulgadas
 */
int16_t HcSr04ReadDistanceInches(us_config device)
{
	/*Función idéntica a HcSr04ReadDistanceCentimeters
	 * Solo cambia la constante ESCALA_CM por ESCALA_INCH*/
	int16_t contador = 0; /*Contador para la cantidad de us*/
	int16_t medicion = 0; /*Valor calculado de la medición*/

	DispararUltrasonido(device.puerto_salida); /*Se genera el pulso que activa el lector*/

		/*Espera a que se genere el pulso de la señal recibida*/
		while(GPIORead(device.puerto_entrada)==false)
		{

		}
		/*Una vez generado el pulso entra incrementa el valor del contador
		 * en 1 cada us*/
		while(GPIORead(device.puerto_entrada))
		{
			DelayUs(1);
			contador = contador+1;
		}

		/*La medición es proporcional al valor alcanzado por el contador según ESCALA_CM*/
		medicion = contador / ESCALA_INCH;

		return medicion;
}



/*==================[end of file]============================================*/




