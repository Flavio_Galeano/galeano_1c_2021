/** @mainpage LecturaUltrasonidoConLEDs.h
 *
 * @section genDesc General Description
 *
 * Documento source del programa medidor de distancias con LEDs.
 *
 * Initials: FG     Name: Flavio Galeano
 * ---------------------------
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN4_2	 	| 	GPIO2[2]	|
 * | 	PIN4_3	 	| 	GPIO2[3]	|
 * | 	ADC0_0	 	| 	ADC_CH1		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210506 v1.0.0 initial version
 */


/*==================[inclusions]=============================================*/
#include "../inc/LecturaUltrasonidoConLEDs.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	LedsOffAll();

	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	uint16_t lectura=0;

	while(1)
	{
		lectura = HcSr04ReadDistanceCentimeters();
		if (lectura<=10)
		{
			LedOn(LED_RGB_G);
		}
		if ((lectura>10)&&(lectura<=20))
		{
			LedOn(LED_RGB_G);
			LedOn(LED_1);
		}
		if ((lectura>20)&&(lectura<=30))
		{
			LedOn(LED_RGB_G);
			LedOn(LED_1);
			LedOn(LED_2);
		}
		if ((lectura>30))
		{
			LedOn(LED_RGB_G);
			LedOn(LED_1);
			LedOn(LED_2);
			LedOn(LED_3);
		}
		DelayMs(100);
		LedsOffAll();

	}

return 0;
}

/*==================[end of file]============================================*/

