/**LecturaUltrasonidoConLEDs.c
 *
 * @section genDesc General Description
 *
 * Documento source del programa medidor de distancias con LEDs.
 *
 *
 *
 * Initials FG     Name Flavio Galeano
 * ---------------------------
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210506 v1.0.0 initial version
 */

#ifndef _LECTURA_ULTRASONIDO_H
#define _LECTURA_ULTRASONIDO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _LECTURA_ULTRASONIDO_H */
