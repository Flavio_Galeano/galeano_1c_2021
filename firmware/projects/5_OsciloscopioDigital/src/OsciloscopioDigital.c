/** @mainpage OsciloscopioDigital.c
 *
 * @section genDesc General Description
 *
 * Documento source del programa de conversión A/D con graficación en osciloscopio digital.
 *
 *
 * Initials: FG     Name: Flavio Galeano
 * ---------------------------
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	CH1 	 	| 	DAC     	|
 * | 	DAC		 	| 	CH1     	|
 * | 	PC  	 	|SERIAL_PORT_PC	|
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210521 v1.0.0 initial version
 */

/*==================[inclusions]=============================================*/
#include "../inc/OsciloscopioDigital.h"       /* <= own header */
#include <string.h>
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*Prototipos de funciones*/
void EnviarDatos(void);
void Conversion(void);
void Writing(void);
void On(void);
void Off(void);
void Lower(void);
void Higher(void);
uint16_t Filtro(uint16_t dato, uint16_t salida_anterior);

/*==================[macros and definitions]=================================*/
#define CH0 1  /*Board connector: DAC pin*/
#define CH1 2  /*Board connector: CH1 pin*/
#define CH2 4  /*Board connector: CH2 pin*/
#define CH3 8  /*Board connector: CH3 pin*/

#define DEC 10 /*Base hexadecimal para el UartItoA*/
#define BAUDIOS 115200 /*Baud rate elegido*/
#define BUFFER_SIZE 231 /*tamaño del ECG*/
#define PI 3.1416
/*==================[internal data definition]===============================*/

uint16_t dato; /*Almacena el valor leido de CH1 en la converisón AD*/
uint8_t i=0; /*Recorre el vector de ECG en la función Writing()*/
uint16_t salida_anterior; /*Para guardar la salida de la iteración anterior en la función Filtro()*/
float fc; /*Frecuencia de corte del filtrado en Filtro()*/
float RC; /*Parámetro utilizado en Filtro()*/
float alfa; /*Parámetro utilizado en Filtro()*/

bool OnOff=false; /*Determina si se envía datos por usb o no*/

analog_input_config senial = {CH1, AINPUTS_BURST_READ, EnviarDatos};  /*Struct de conversión AD*/
timer_config timerA = {TIMER_A, 4, Writing};  /*Struct de timerA para la función Writing()*/
timer_config timerB = {TIMER_B, 2, Conversion}; /*Struct de timerB para la función Conversion()*/
serial_config usb = {SERIAL_PORT_PC, BAUDIOS, NULL}; /*struct de uart*/

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
}; /*ECG digitalizado*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/** @fn void SystemInitializer(void)
 * @brief Inicialización de funciones y periféricos.
 * @param[in] void
 */
void SystemInitializer(void)

{
	SystemClockInit();
	AnalogInputInit(&senial);
	AnalogOutputInit();
	TimerInit(&timerA);
	TimerInit(&timerB);
	UartInit(&usb);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, On);
	SwitchActivInt(SWITCH_2, Off);
	SwitchActivInt(SWITCH_3, Lower);
	SwitchActivInt(SWITCH_4, Higher);
}

/** @fn void VariableInit(void)
 * @brief Define los valores iniciales de fc, RC, alfa y salida_anterior.
 * @param[in] void
 */
void VariableInit(void)
{
	fc=1;
	RC=1/(2*PI*fc);
	alfa=0.02/(RC+0.02);
	salida_anterior=ecg[0];
}

/** @fn void Writing(void)
 * @brief Disparado por timer. Genera una señal analógica a partir del ECG digitalizado.
 * @param[in] void
 */
void Writing(void)
{
	AnalogOutputWrite(ecg[i]);
	i++;
	if(i==BUFFER_SIZE)
	{
		i=0;
	}
}

/** @fn void Conversion(void)
 * @brief Disparado por timer. Comienza la conversión.
 * @param[in] void
 */
void Conversion(void)
{
	AnalogStartConvertion();
}

/** @fn void EnviarDatos(void)
 * @brief Lee el canal 1 y envía datos por UART
 * @param[in] void
 */
void EnviarDatos(void)
{
	char string2[] = "\r";
	AnalogInputRead(CH1, &dato);
	uint16_t salida;
	salida=Filtro(dato, salida_anterior);
	salida_anterior=salida;
	if(OnOff)
	{
		UartSendString(SERIAL_PORT_PC, UartItoa(salida, DEC)); /*Se envía el dato a la PC*/
		UartSendString(SERIAL_PORT_PC, string2); /*Se envía el caracter de fin de linea a la PC*/
	}
}

/** uint16_t Filtro(uint16_t dato, uint16_t salida_anterior)
 * @brief Aplica un filtrado pasa-bajos utilizando una entrada actual y una salida de la iteración anterior.
 * @param[in] void
 */
uint16_t Filtro(uint16_t dato, uint16_t salida_anterior)
{
	uint16_t salida;
	salida = salida_anterior + alfa * (dato - salida_anterior);
	return salida;
}

/** void On(void)
 * @brief Habilita la transferencia de datos por USB de la placa a la PC
 * @param[in] void
 */
void On(void)
{
	if(OnOff==false)
	{
		OnOff=true;
	}
}

/** void Off(void)
 * @brief Deshabilita la transferencia de datos por USB de la placa a la PC
 * @param[in] void
 */
void Off(void)
{
	if(OnOff==true)
	{
		OnOff=false;
	}
}

/** void Lower(void)
 * @brief Reduce la frecuencia de corte en 0.1 Hz.
 * @param[in] void
 */
void Lower(void)
{
	if(fc>=0.1)
	{
		fc=fc-0.1;
		RC=1/(2*PI*fc);
		alfa=0.02/(RC+0.02);
	}
}

/** void Higher(void)
 * @brief Aumenta la frecuencia de corte en 0.1 Hz.
 * @param[in] void
 */
void Higher(void)
{
	if(fc<=3.0)
	{
		fc=fc+0.1;
		RC=1/(2*PI*fc);
		alfa=0.02/(RC+0.02);
	}
}






/**
 * @fn main
 * @brief Función main
 * @param[in] void
 * @return 0
 */
int main(void)
{
	SystemInitializer();
	VariableInit();
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	while(1)
	{

	}
	return 0;
}

/*==================[end of file]============================================*/

