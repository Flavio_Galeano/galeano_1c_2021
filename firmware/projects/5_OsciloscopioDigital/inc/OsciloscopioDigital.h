/**OsciloscopioDigital.h
 *
 * @section genDesc General Description
 *
 * Documento header del programa de conversión A/D con graficación en osciloscopio digital.
 *
 *
 *
 * Initials FG     Name Flavio Galeano
 * ---------------------------
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210521 v1.0.0 initial version
*/

#ifndef PROJECTS_5_OSCILOSCOPIODIGITAL_INC_OSCILOSCOPIODIGITAL_H_
#define PROJECTS_5_OSCILOSCOPIODIGITAL_INC_OSCILOSCOPIODIGITAL_H_


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* PROJECTS_5_OSCILOSCOPIODIGITAL_INC_OSCILOSCOPIODIGITAL_H_ */
