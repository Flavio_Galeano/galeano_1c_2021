/** @mainpage LecturaUltrasonido.c
 *
 * @section genDesc General Description
 *
 * Documento source del programa medidor de distancias con display LCD implementado con timers y conexión de puerto serie.
 *
 *
 *
 * Initials: FG     Name: Flavio Galeano
 * ---------------------------
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN4_2	 	| 	GPIO2[2]	|
 * | 	PIN4_3	 	| 	GPIO2[3]	|
 * | 	ADC0_0	 	| 	ADC_CH1		|
 * | 	PIN3	 	| 	GND			|
 * | 	PC  	 	|SERIAL_PORT_PC	|
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210506 v1.0.0 initial version
 */

/*==================[inclusions]=============================================*/
#include "../inc/LecturaUltrasonido.h"       /* <= own header */
#include <string.h>
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "lcditse0803.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUDIO 115200
#define MASK_H 72         	/*H en ASCII*/
#define MASK_O 79			/*O en ASCII*/
#define MASK_h 104			/*h en ASCII*/
#define MASK_o 111			/*o en ASCII*/

/*==================[internal data definition]===============================*/
bool encendido; /*Si encendido==false, el programa entra en bucle sin operaciones.
				  Si encendido==true el programa funciona en el bucle de trabajo*/
bool isHold;  /*Si isHold==false, se está midiendo continuamente. Si isHold==true mantiene la medición*/


/*==================[internal functions declaration]=========================*/

/**
 * @fn OnOff
 * @brief Encendido/apagado del programa
 * @param[in] void
 * @return void
 */
void OnOff(void)
{
	encendido = !encendido; /*Invierte el valor de la variable de encendido*/
	isHold = false; /*Al reiniciar el programa éste debe estar en modo lectura*/

	if (encendido) /*Si se prende el dispositivo, se prende el led verde*/
	{
		LedOn(LED_RGB_G);
	}
	if(!encendido) /*Si se apaga el dispositivo, se apagan todos los leds y el display LCD*/
	{
		LedsOffAll();
		LcdItsE0803Off();
	}
}


/**
 * @fn HoldOnOff
 * @brief Activa/desactiva el modo hold de lectura
 * @param[in] void
 * @return void
 */
void HoldOnOff(void)
{
	isHold = !isHold; /*Invierte el valor de la variable de hold*/
}


/**
 * @fn USBRead
 * @brief Recibe datos desde la PC y ejecuta comandos según lo recibido
 * @param[in] void
 * @return void
 */
void USBRead (void)
{
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);  /*Lee el dato desde la PC*/
	if((dat==MASK_H)||(dat==MASK_h))	/*Con "H" apaga o prende el medidor*/
	{
		HoldOnOff();
	}
	if(dat==MASK_O||(dat==MASK_o))		/*Con "O" Mantiene o no la medición*/
	{
		OnOff();
	}
}

/**
 * @fn Lectura
 * @brief Lleva a cabo la medición por ultrasonido y comunica los datos tanto por display como por PC
 * @param[in] void
 * @return void
 */
void Lectura(void)
{
	if(encendido)
	{
		uint16_t lectura;
		lectura = HcSr04ReadDistanceCentimeters(); /*Se toma la lectura*/
		if (!isHold) /*Si no se está en estado "Hold":*/
		{
			LedOff(LED_1);
			LcdItsE0803Write(lectura); /*Se escribe en el display LCD*/
			char string1[]="\r\n";
			char string2[]="O.L.";
			if(lectura<400)
			{
				UartSendString(SERIAL_PORT_PC, UartItoa(lectura, 10)); /*Se envía el dato a la PC*/
			}
			else /*Si la lectura es mayor a 400*/
			{
				UartSendString(SERIAL_PORT_PC, string2); /*Se envía un mensaje de "sobrecargado" (O.L.)*/
			}
			UartSendString(SERIAL_PORT_PC, string1); /*Se envía el comando de fin de linea*/

		}
		if (isHold)
		{
			LedOn(LED_1);
		}
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


/**
 * @fn main
 * @brief Función main
 * @param[in] void
 * @return 0
 */
int main(void)
{
	/*Apaga todos los leds antes de comenzar*/
	LedsOffAll();

	/*Definición de timer y uart*/
	timer_config Timer;
		Timer.timer=TIMER_A;
		Timer.period=TIMER_A_1ms_TICK/2;
		Timer.pFunc=Lectura;

	serial_config usb;
		usb.port=SERIAL_PORT_PC;
		usb.baud_rate=BAUDIO;
		usb.pSerial=USBRead;

	/*Inicializaciones*/
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SystemClockInit();
	LedsInit();
	LcdItsE0803Init();
	SwitchesInit();
	TimerInit(&Timer);
	UartInit(&usb);

	/*Se declaran las interrupciones con los switch*/
	SwitchActivInt(SWITCH_1, OnOff); /*Switch 1 activa la función OnOff*/
	SwitchActivInt(SWITCH_2, HoldOnOff); /*Switch 2 activa la función HoldOnOff*/

	encendido = false;
	isHold = false;  /*Si isHold==false, se está midiendo continuamente. Si isHold==true mantiene la medición*/
	TimerStart(Timer.timer); /*Comienza a correr el timer*/


	while(1)
	{

	}

return 0;
}

/*==================[end of file]============================================*/

