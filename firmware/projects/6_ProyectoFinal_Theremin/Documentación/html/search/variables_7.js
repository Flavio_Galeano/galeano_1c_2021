var searchData=
[
  ['panaloginput',['pAnalogInput',['../structanalog__input__config.html#a2298b64ab1835d56f927cb94137f36ad',1,'analog_input_config']]],
  ['period',['period',['../structtimer__config.html#a258408d6d5d13a24bfa5211d81ce1682',1,'timer_config::period()'],['../_theremin_8c.html#ae08ac1ff9a62c213c5768ca3a538e546',1,'period():&#160;Theremin.c']]],
  ['pfunc',['pFunc',['../structtimer__config.html#a9cead290357aaae808d4db4ab87784df',1,'timer_config']]],
  ['port',['port',['../structserial__config.html#a2fa54f9024782843172506fadbee2ac8',1,'serial_config']]],
  ['pserial',['pSerial',['../structserial__config.html#a1944cd6d24e8b238d8e728d0cf201541',1,'serial_config']]],
  ['puerto_5fentrada',['puerto_entrada',['../structus__config.html#aaf73489b9980646865e119a0f739e076',1,'us_config']]],
  ['puerto_5fsalida',['puerto_salida',['../structus__config.html#aeda5c4906af2f86b41b730fd3a9ea2a5',1,'us_config']]]
];
