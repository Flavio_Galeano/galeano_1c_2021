var searchData=
[
  ['theremin_2ec',['Theremin.c',['../index.html',1,'']]],
  ['theremin_2ec',['Theremin.c',['../_theremin_8c.html',1,'']]],
  ['timer',['timer',['../structtimer__config.html#a0ff73b1428e9cd11a4f7a6cf46b7b550',1,'timer_config::timer()'],['../group___timer.html',1,'(Namespace global)']]],
  ['timer_2eh',['timer.h',['../timer_8h.html',1,'']]],
  ['timer_5fa',['TIMER_A',['../group___timer.html#ga47d0eac91b49052bcd964223eb8eeffc',1,'timer.h']]],
  ['timer_5fa_5f1ms_5ftick',['TIMER_A_1ms_TICK',['../group___timer.html#ga162d13fe964cde50a95d80a88349be7d',1,'timer.h']]],
  ['timer_5fb',['TIMER_B',['../group___timer.html#ga394289ce488144cf553f2193ad4f38c3',1,'timer.h']]],
  ['timer_5fb_5f1ms_5ftick',['TIMER_B_1ms_TICK',['../group___timer.html#gaef8c2dd280d8e80f0613ebf320eb2838',1,'timer.h']]],
  ['timer_5fc',['TIMER_C',['../group___timer.html#ga7f80ae7115920d2e2c6e57670471a216',1,'timer.h']]],
  ['timer_5fc_5f1us_5ftick',['TIMER_C_1us_TICK',['../group___timer.html#ga32cf8edb66a6be9668cbeaea51cfabcf',1,'timer.h']]],
  ['timer_5fconfig',['timer_config',['../structtimer__config.html',1,'']]],
  ['timer_5fplay',['timer_play',['../_theremin_8c.html#a2f666cba620d06633044e78ee183bceb',1,'Theremin.c']]],
  ['timer_5fread',['timer_read',['../_theremin_8c.html#aa4a60f8d8eca014cc2e2d35fdba43bdc',1,'Theremin.c']]],
  ['timerinit',['TimerInit',['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'timer.h']]],
  ['timerreset',['TimerReset',['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'timer.h']]],
  ['timerstart',['TimerStart',['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'timer.h']]],
  ['timerstop',['TimerStop',['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'timer.h']]]
];
