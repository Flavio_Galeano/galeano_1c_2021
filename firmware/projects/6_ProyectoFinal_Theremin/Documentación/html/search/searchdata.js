var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvw",
  1: "astu",
  2: "aghlstu",
  3: "aceghlmrstuv",
  4: "bcfilmopstuw",
  5: "gils",
  6: "gls",
  7: "abcdef",
  8: "dglstu",
  9: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Estructuras de Datos",
  2: "Archivos",
  3: "Funciones",
  4: "Variables",
  5: "Enumeraciones",
  6: "Valores de enumeraciones",
  7: "defines",
  8: "Grupos",
  9: "Páginas"
};

