/**Theremin.h
 *
 * @section genDesc General Description
 *
 * Documento header del programa de theremín por ultrasonido
 *
 *
 *
 * Initials FG     Name Flavio Galeano
 * ---------------------------
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210608 v1.0.0 initial version
*/

#ifndef PROJECTS_6_PROYECTOFINAL_THEREMIN_INC_THEREMIN_H_
#define PROJECTS_6_PROYECTOFINAL_THEREMIN_INC_THEREMIN_H_

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/



#endif /* PROJECTS_6_PROYECTOFINAL_THEREMIN_INC_THEREMIN_H_ */
