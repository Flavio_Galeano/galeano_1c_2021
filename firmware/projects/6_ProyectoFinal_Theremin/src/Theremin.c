/** @mainpage Theremin.c
 *
 * @section genDesc General Description
 *
 * Documento source del programa de theremín por ultrasonido
 *
 *
 * Initials: FG     Name: Flavio Galeano
 * ---------------------------
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |	VCC 1		|    P1_2      	|
 * | 	GND 1	 	|    P1_4      	|
 * | 	Trigger 1 	|    P2_32     	|
 * | 	Echo 1		|    P2_34    	|
 * |	VCC 2		|    P2_2      	|
 * | 	GND 2	 	|    P2_3      	|
 * | 	Trigger 2 	|    P2_37     	|
 * | 	Echo 2	    |	 P2_35		|
 * | PARLANTE_VCC	| 	 P1_15     	|
 * | PARLANTE_GND	| 	 P1_16     	|
 * | 	PC  	 	|SERIAL_PORT_PC	|
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210608 v1.0.0 initial version
 */

/*==================[inclusions]=============================================*/
#include "../inc/Theremin.h"       /* <= own header */
#include "../inc/Violin_G.h"
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "hc_sr4.h"

/*Prototipos de funciones*/
void Reproducir(void);
void Lectura(void);
uint16_t CalculoPeriodo(us_config lector_freq);
float CalculoAmplitud(us_config lector_amp);
void EnviarDatos();
void Encender(void);


/*==================[macros and definitions]=================================*/
#define CH0 1  /*Board connector: DAC pin*/
#define BAUDIOS 115200 /*Baud rate elegido*/
#define DEC 10 /*Base numérica con la cual enviar los datos por USB*/
#define DISTANCIA_MIN 25 /*Distancia mínima (en mm) a la que se pueden tomar datos en el lector de ultrasonido*/
#define DISTANCIA_MAX 1005 /*Distancia máxima (en mm) a la que se pueden tomar datos en el lector de ultrasonido de frecuencias*/
#define DISTANCIA_MAX_AMP 400 /*Distancia máxima (en mm) a la que se pueden tomar datos en el lector de ultrasonido de amplitud*/
#define FREQ_MIN 350 /*Frecuencia del tono mas bajo posible de emitir*/
#define FACTOR_LINEAL 0.002500 /*Factor matemático utilizado durante el cálculo de la amplitud de salida*/

/*==================[internal data definition]===============================*/
uint16_t freq; /*Frecuencia fundamental del tono emitido por el DAC*/
float period; /*Periodo en que se modifica la salida del DAC*/
bool OnOff=false; /*Determina si se envía datos por parlante y usb o no*/
uint16_t contador = 0; /*Variable que recorre el vector de la señal digitaliziada y determina que elemento de vector se emite por el DAC*/
uint8_t contador_usb = 0; /*Variable que reduce el ritmo de envío de datos de USB a 1 por segundo*/
float factor_amp; /*Factor por el cual se multiplica la señal de salida para reducir su amplitud, modificando el volumen*/
uint16_t *wave; /*Puntero que apunta al vector que corresponde a la señal digitalizada a utilizar*/
uint16_t size; /*Tamaño del vector que aloja la señal digitalizada*/

timer_config timer_play;  /*Struct de timerA para la función Reproducir()*/
timer_config timer_read; /*Struct de timerA para la función Lectura()*/
serial_config usb = {SERIAL_PORT_PC, BAUDIOS, NULL}; /*struct de uart*/
us_config lector_freq = {GPIO_T_FIL2, GPIO_T_FIL3}; /*struct del ultrasonido para el lector de frecuencias*/
us_config lector_amp = {GPIO_1, GPIO_3}; /*struct del ultrasonido para el lector de amplitud*/


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/** @fn void SystemInitializer(void)
 * @brief Inicialización de funciones y periféricos.
 * @param[in] void
 */
void SystemInitializer(void)
{
	SystemClockInit();
	AnalogOutputInit();
	SwitchesInit();
	LedsInit();
	HcSr04Init(lector_freq);
	HcSr04Init(lector_amp);
	UartInit(&usb);
	SwitchActivInt(SWITCH_1, Encender);
}

/** @fn void VariableInit(void)
 * @brief Define los valores iniciales de frq, period y los parámetros del timer.
 * @param[in] void
 */
void VariableInit(void)
{
	wave=Violin;
	size=sizeof(wave);
	freq=1;
	period = 1000000/(freq*size);
	factor_amp=1;

	timer_play.timer = TIMER_C;
	timer_play.period = period;
	timer_play.pFunc = Reproducir;

	timer_read.timer = TIMER_B;
	timer_read.period = 40;
	timer_read.pFunc = Lectura;
}

/** @fn void Encender(void)
 * @brief Enciende o apaga el sistema
 * @param[in] void
 */
void Encender(void)
{
	OnOff=!OnOff;
	if(OnOff)
	{
		LedOn(LED_RGB_B);
	}
	if(!OnOff)
	{
		LedOff(LED_RGB_B);
	}
}

/** @fn void Lectura(void)
 * @brief LLamado por el timer B. Define los nuevos parámetros de periodo y amplitud.
 * @param[in] void
 */
void Lectura(void)
{
	period=CalculoPeriodo(lector_freq);
	factor_amp=CalculoAmplitud(lector_amp);
	TimerReset(timer_play.timer);
	timer_play.period = period;
	if(contador_usb==20)
	{
		if(OnOff)
		{
			EnviarDatos();
		}
		contador_usb=0;
	}
	contador_usb=contador_usb+1;
}

/** @fn void Reproducir(void)
 * @brief Genera la salida de la señal analógica a través del DAC
 * @param[in] void
 */
void Reproducir (void)
{
	if(OnOff)
	{
		uint16_t salida;
		salida = *(wave+contador);
		salida = salida/factor_amp;
		AnalogOutputWrite(salida);
		contador++;
		if(contador==size)
		{
			contador=0;
		}
	}
}

/** @fn uint16_t CalculoPeriodo(us_config lector_freq)
 * @brief Toma una lectura del lector de ultrasonido de frecuencias y calcula el nuevo periodo de timer
 * @param[in] us_config lector_freq
 */
uint16_t CalculoPeriodo(us_config lector_freq)
{
	float lectura;
	uint16_t frecuencia;
	uint16_t periodo;

	lectura = HcSr04ReadDistanceMilimeters(lector_freq);
	frecuencia = FREQ_MIN*size + 2.0*lectura*size;
	periodo = 1000000/(frecuencia);

	if((lectura>DISTANCIA_MIN)&&(lectura<DISTANCIA_MAX))
	{
		return periodo;
	}
	else
	{
		return 1;
	}
}

/** @fn float CalculoAmplitud(us_config lector_amp)
 * @brief Toma una lectura del lector de ultrasonido de amplitud y calcula el factor con el que multiplicar la señal analógica para determinar el volumen de salida
 * @param[in] us_config lector_amp
 */
float CalculoAmplitud(us_config lector_amp)
{
	float lectura;
	float factor;

	lectura = HcSr04ReadDistanceMilimeters(lector_amp);
	factor = lectura*FACTOR_LINEAL + 0.05;

	if((lectura>DISTANCIA_MIN)&&(lectura<DISTANCIA_MAX_AMP))
	{
		return factor;
	}
	else
	{
		return 0;
	}
}

/** @fn void EnviarDatos(void)
 * @brief Envía valores de frecuencia y amplitud por usb
 * @param[in] void
 */
void EnviarDatos(void)
{
	uint16_t frecuencia;
	uint8_t porcentaje_amp;
	frecuencia = 1000000/period;
	porcentaje_amp = 100-(100*(factor_amp)/(DISTANCIA_MAX_AMP*FACTOR_LINEAL + 0.05));

	char string1[] = {"Frecuencia: "};
	char string2[] = {"|--|--|--| "};
	char string3[] = {"Volumen: "};
	char string4[] = {"%"};
	char string5[] = {"\r\n"};
	char string_zero[] = {"0"};


	UartSendString(SERIAL_PORT_PC, string1);
	UartSendString(SERIAL_PORT_PC, UartItoa(frecuencia, DEC));
	UartSendString(SERIAL_PORT_PC, string2);
	UartSendString(SERIAL_PORT_PC, string3);
	if(factor_amp==0)
	{
		UartSendString(SERIAL_PORT_PC, string_zero);
	}
	else
	{
		UartSendString(SERIAL_PORT_PC, UartItoa(porcentaje_amp, DEC));
	}
	UartSendString(SERIAL_PORT_PC, string4);
	UartSendString(SERIAL_PORT_PC, string5);
}




/**
 * @fn main
 * @brief Función main
 * @param[in] void
 * @return 0
 */

int main(void)
{
	SystemInitializer();
	VariableInit();

	TimerInit(&timer_play);
	TimerStart(TIMER_A);
	TimerInit(&timer_read);
	TimerStart(TIMER_B);

	while(1)
	{

	}
	return 0;
}
