/** @mainpage LecturaUltrasonido.c
 *
 * @section genDesc General Description
 *
 * Documento source del programa medidor de distancias con display LCD.
 *
 *
 *
 * Initials: FG     Name: Flavio Galeano
 * ---------------------------
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN4_2	 	| 	GPIO2[2]	|
 * | 	PIN4_3	 	| 	GPIO2[3]	|
 * | 	ADC0_0	 	| 	ADC_CH1		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210506 v1.0.0 initial version
 */

/*==================[inclusions]=============================================*/
#include "../inc/LecturaUltrasonido.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "lcditse0803.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
bool encendido; /*Si encendido==false, el programa entra en bucle sin operaciones.
				  Si encendido==true el programa funciona en el bucle de trabajo*/
bool isHold;  /*Si isHold==false, se está midiendo continuamente. Si isHold==true mantiene la medición*/


/*==================[internal functions declaration]=========================*/

/**
 * @fn OnOff
 * @brief Encendido/apagado del programa
 * @param[in] void
 * @return void
 */
void OnOff(void)
{
	encendido = !encendido; /*Invierte el valor de la variable de encendido*/
	isHold = false; /*Al reiniciar el programa éste debe estar en modo lectura*/
}


/**
 * @fn HoldOnOff
 * @brief Activa/desactiva el modo hold de lectura
 * @param[in] void
 * @return void
 */
void HoldOnOff(void)
{
	isHold = !isHold; /*Invierte el valor de la variable de hold*/
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


/**
 * @fn main
 * @brief Función main
 * @param[in] void
 * @return 0
 */
int main(void)
{
	/*Apaga todos los leds antes de comenzar*/
	LedsOffAll();

	/*Inicializaciones*/
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SystemClockInit();
	LedsInit();
	LcdItsE0803Init();
	SwitchesInit();

	/*Se declaran las interrupciones con los switch*/
	SwitchActivInt(SWITCH_1, OnOff); /*Switch 1 activa la función*/
	SwitchActivInt(SWITCH_2, HoldOnOff);

	encendido = false;
	isHold = false;  /*Si isHold==false, se está midiendo continuamente. Si isHold==true mantiene la medición*/
	uint16_t lectura=0;

	while(1)
	{
		while(!encendido)
		{

		}

		LedOn(LED_RGB_G);

		while(encendido)
		{
			lectura = HcSr04ReadDistanceCentimeters();
			DelayUs(100);
			if(!isHold)
			{
				LedOff(LED_1);
				LcdItsE0803Write(lectura);
			}
			if(isHold)
			{
				LedOn(LED_1);
			}
		}
		LedsOffAll();
		LcdItsE0803Off();
	}

return 0;
}

/*==================[end of file]============================================*/

