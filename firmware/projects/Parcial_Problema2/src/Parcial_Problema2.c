/**
 * @mainpage Parcial_Problema2.c
 *
 * @section genDesc General Description
 *
 * Documento source del programa de control de fase digital
 *
 *
 * Initials: FG     Name: Flavio Galeano
 * ---------------------------
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1	    	|   EDU-CIAA	|
 * |:------------------:|:--------------|
 * | Dispositivo de    	|      CH1     	|
 * |acondicionamiento  	| 	        	|
 * |		Triac	  	| 	   DAC     	|
 * | 	PC  	 		|SERIAL_PORT_PC	|
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v1.0.0 initial version
 */

/*==================[inclusions]=============================================*/
#include <string.h>

#include "../../Parcial_Problema1/inc/Parcial_Problema1.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*Prototipos de funciones*/
void ConversionAD(void);
void Deteccion(void);
void DispararPulso();
void USBRead(void);



/*==================[macros and definitions]=================================*/
#define CH0 1  /*Board connector: DAC pin*/
#define BAUDIOS 115200 /*Baud rate elegido*/
#define RANGO_DA 1024 /*Rango en bits del conversor DA*/
#define AMPLITUD 1024 /*Amplitud (en rango de bits) del pulso disparado. Equivale a 5 V*/
#define MASK_S 83         	/*S en ASCII*/
#define MASK_B 66			/*B en ASCII*/
#define MASK_s 115			/*s en ASCII*/
#define MASK_b 98			/*b en ASCII*/

/*==================[internal data definition]===============================*/

analog_input_config senial = {CH1, AINPUTS_BURST_READ, Deteccion};  /*Struct de conversión AD*/
timer_config timer = {TIMER_A, 1, ConversionAD};  /*Struct de timerA para la función*/
serial_config usb = {SERIAL_PORT_PC, BAUDIOS, USBRead}; /*struct de uart*/

uint16_t dato; /*Almacena el valor leido de CH1 en la converisón AD*/
uint16_t dato_anterior; /*Almacena el valor leido de CH1 en la converisón AD para ser usado en el ciclo siguiente*/
bool primera_lectura = true; /*Afecta el desarrollo de la función Deteccion() cuando se realiza la primera lectura*/
bool StopPulse; /*Bandera utilizada para que, luego de disparado el pulso, la salida del DAC vuelva a 0 en la siguiente llamada de Deteccion()*/
uint8_t R0=10; /*Valor que determina cuando se disparará el pulso*/
uint16_t contador=0; /*Compara con R0 para determinar cuando disparar el pulso*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/** @fn void SystemInitializer(void)
 * @brief Inicialización de funciones y periféricos.
 * @param[in] void
 */
void SystemInitializer(void)
{
	SystemClockInit();
	AnalogInputInit(&senial);
	AnalogOutputInit();
	TimerInit(&timer);
	UartInit(&usb);
}

/** @fn void ConversionAD(void)
 * @brief Disparado por timer. Comienza la conversión.
 * @param[in] void
 */
void ConversionAD(void)
{
	AnalogStartConvertion();
}

/** @fn void Deteccion(void)
 * @brief Disparado por ConversionAD(). Detecta los cruces por cero y determina cuando disparar el pulso para el triac.
 * @param[in] void
 */
void Deteccion(void)
{
	AnalogInputRead(CH1, &dato); /*Lectura del dato*/

	if(primera_lectura) /*El programa solo entra acá en el primer llamado de esta función Deteccion()*/
	{
		dato_anterior=dato; /*En esta primera lectura solamente se guarda el valor leido para usarlo en el siguiente ciclo*/
		primera_lectura=false; /*Ya no habrá mas primeras lecturas*/
	}

	else /*El resto de llamados el programa sigue esta ruta*/
	{
		if((dato_anterior<RANGO_DA/2)&&(dato>=RANGO_DA/2) || (dato_anterior>=RANGO_DA/2)&&(dato<RANGO_DA/2))
		{
						/*Este "If" compara el valor leído en este llamado de Detección() con el leído en el llamado anterior.*/
			contador=0; /*Cuando uno de los valores anteriores está por encima de la mitad del valor máximo de la señal de entrada*/
		 				/*y el valor actual está por debajo (o viceversa) se reinicia la variable contador.*/
						/*Este contador se incrementa en uno en cada llamado de Deteccion(), y al igualarse a R0 dispara el pulso (ver abajo)*/
		}
		if(StopPulse)
		{
			uint16_t zero = 0;

		 							/*Cuando se llamó la función DispararPulso() en la iteración anterior, se seteó en "true" esta variable StopPulse.*/
			AnalogOutputWrite(zero);/*El programa entonces, solo entra en este if en la iteración siguiente a un disparo*/
			StopPulse = false;		/*Se pone la salida del DAC en 0 y StopPulse se vuelve a false para que el programa no entre en este If en el*/
									/*próximo llamado de Deteccion().*/
		}
		if(contador==R0) /*Cuando contador llegue al valor establecido de R0... */
		{
			DispararPulso(); /*...se dispara el pulso*/
		}
		contador++; /*El valor de contador incrementa acumulativamente en cada ciclo hasta la detección de un cruce por cero, cuando se reinicia*/
		dato=dato_anterior; /*Se guarda el dato para ser utilizado el próximo ciclo*/
	}
}

/** @fn void DispararPulso(void)
 * @brief Dispara el pulso de 5V y 1 ms para el triac.
 * @param[in] void
 */
void DispararPulso()
{
	AnalogOutputWrite(AMPLITUD); /*Setéa el valor de salida del DAC en 5 V (Amplitud es el máximo valor posible del conversor)*/
	StopPulse = true; /*La bandera StopPulse se activa para ser usada en el próximo llamado de Deteccion()*/
}


/** @fn void USBRead(void)
 * @brief Toma datos desde el puerto USB para modificar R0.
 * @param[in] void
 */
void USBRead(void)
{
	uint8_t dat; /*Variable en la que se guarda el dato leído*/
	UartReadByte(SERIAL_PORT_PC, &dat);  /*Lee el dato desde la PC*/
	if((dat==MASK_S)||(dat==MASK_s))	/*Con "S" aumenta el valor de R0*/
	{
		if(R0<20)
		{
			R0=R0+5;
		}
	}
	if(dat==MASK_B||(dat==MASK_b))		/*Con "B" Disminuye el valor de R0*/
	{
		if(R0>0)
		{
			R0=R0-5;
		}
	}
}


/**
 * @fn main
 * @brief Función main
 * @param[in] void
 * @return 0
 */
int main(void)
{
	SystemInitializer();
	TimerStart(TIMER_A);
	while(1)
	{

	}
	return 0;
}

