/**
 * @mainpage Parcial_Problema1.c
 *
 * @section genDesc General Description
 *
 * Documento source del programa de cálculo de velocidad por ultrasonido
 *
 *
 * Initials: FG     Name: Flavio Galeano
 * ---------------------------
 *
 * @section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * |hc_sr4 ECHO	 	| 	GPIO_T_FIL2	|
 * |hc_sr4 TRIGGER	| 	GPIO_T_FIL3	|
 * |hc_sr4 VCC	 	| 	5V			|
 * |hc_sr4 GND	 	| 	GND			|
 * | 	PC  	 	|SERIAL_PORT_PC	|
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v1.0.0 initial version
 */

/*==================[inclusions]=============================================*/
#include <string.h>

#include "../../Parcial_Problema1/inc/Parcial_Problema1.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "hc_sr4.h"

/*Prototipos de las funciones*/
void OnOff(void);
void Lectura(void);
void LedsVel(uint16_t vel);

/*==================[macros and definitions]=================================*/
#define BAUDIOS 115200 /*Baud rate elegido*/
#define PERIODO 0.1 /*Tiempo entre lecturas en [s]*/

/*==================[internal data definition]===============================*/

bool encendido=true; /*Habilita el funcionamiento de la función Lectura()*/
uint16_t lectura_anterior=0; /*Guarda el valor de la variable Lectura para ser utilizada en el llamado siguiente*/
timer_config timer = {TIMER_A, 100, Lectura};  /*Struct de timer para la función*/
serial_config usb = {SERIAL_PORT_PC, BAUDIOS, NULL}; /*struct de uart*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/** @fn void SystemInitializer(void)
 * @brief Inicialización de funciones y periféricos.
 * @param[in] void
 */
void SystemInitializer(void)
{
	SystemClockInit();
	TimerInit(&timer);
	UartInit(&usb);
	LedsInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, OnOff);
}

/** @fn void OnOff(void)
 * @brief Habilita o deshabilita la lectura de datos desde el sensor de ultrasonido.
 * @param[in] void
 */
void OnOff(void)
{
	encendido=!encendido; /*Cambia el estado de la variable "encendido". Afecta la función Lectura().*/
	if(encendido)
	{
		LedOn(LED_RGB_B); /*Cuando el sistema se enciende activa el LED azul*/
	}
	if(!encendido)
	{
		LedOff(LED_RGB_B); /*Cuando el sistema se apaga desactiva el LED azul*/
	}
}

/** @fn void Lectura(void)
 * @brief Realiza el sensado de dstancias por ultrasonido y a partir de estos calcula velocidad. Envía datos por USB.
 * @param[in] void
 */
void Lectura(void)
{
	if(encendido)
	{
		uint16_t lectura_cm; /*El sensor devuelve la lectura en cm*/
		float lectura;
		uint16_t vel;
		lectura_cm = HcSr04ReadDistanceCentimeters(); /*Se toma la lectura*/
		lectura = lectura_cm/100; /*Se la transforma a metros*/
		vel = (lectura - lectura_anterior)/PERIODO; /*Se calcula la velocidad instantanea*/
		lectura_anterior = lectura; /*Se guarda la lectura para el llamado siguiente*/
		char string[]="m/s \r\n";
		UartSendString(SERIAL_PORT_PC, UartItoa(lectura, 10)); /*Se envía la lectura*/
		UartSendString(SERIAL_PORT_PC, string); /*Se envía la unidad y el salto de linea*/
		LedsVel(vel); /*Llama a función LedsVel() (Definida abajo)*/
	}
}

/**/

/** @fn void Lectura(uint16_t vel)
 * @brief Determina cuales LEDs se encienden según el cálculo de velocidad.
 * @param[in] uint16_t vel
 */
void LedsVel(uint16_t vel)
{
	LedsOffAll();
	if(vel<3)
	{
		LedOn(LED_3); /*Si la velocidad es menor a 3 enciende el LED 3*/
	}
	if((vel>=3)&&(vel<8))
	{
		LedOn(LED_2); /*Si la velocidad es entre 3 y 8 enciende el LED 2*/
	}
	if(vel>=8)
	{
		LedOn(LED_1); /*Si la velocidad es mayor a 8 enciende el LED 1*/
	}
}

/**
 * @fn main
 * @brief Función main
 * @param[in] void
 * @return 0
 */
int main(void)
{
	SystemInitializer();
	LedsOffAll(); /*Apaga todos los leds antes de comenzar*/
	TimerStart(TIMER_A);
	while(1)
	{

	}
	return 0;
}

