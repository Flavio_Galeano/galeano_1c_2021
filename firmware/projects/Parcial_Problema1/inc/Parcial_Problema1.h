/**Parcial_Problema1.c.h
 *
 * @section genDesc General Description
 *
 * Documento header del programa de cálculo de velocidad por ultrasonido
 *
 *
 *
 * Initials FG     Name Flavio Galeano
 * ---------------------------
 *
 *
 * @section modification history (new versions first)
 * -----------------------------------------------------------
 * 20210609 v1.0.0 initial version
*/

#ifndef PROJECTS_PARCIAL_PROBLEMA1_INC_PARCIAL_PROBLEMA1_H_
#define PROJECTS_PARCIAL_PROBLEMA1_INC_PARCIAL_PROBLEMA1_H_

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/



#endif /* PROJECTS_PARCIAL_PROBLEMA1_INC_PARCIAL_PROBLEMA1_H_ */
