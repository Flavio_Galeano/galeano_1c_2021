var hc__sr4_8c =
[
    [ "ESCALA_CM", "hc__sr4_8c.html#adbbbae662795a1a596677d2424585a5b", null ],
    [ "ESCALA_INCH", "hc__sr4_8c.html#a4823e4e705893f2a64c4354953206861", null ],
    [ "DispararUltrasonido", "hc__sr4_8c.html#a922f7c953b9de5a91875f48ceb5581aa", null ],
    [ "HcSr04Init", "hc__sr4_8c.html#a9d26cc017fe45c607d08231ebffb46c4", null ],
    [ "HcSr04ReadDistanceCentimeters", "hc__sr4_8c.html#aa1b3e3a72f081db98eafa97000197a79", null ],
    [ "HcSr04ReadDistanceInches", "hc__sr4_8c.html#ac94cf11dbb224fead4118c8fbc7199c5", null ],
    [ "puerto_entrada", "hc__sr4_8c.html#aaf73489b9980646865e119a0f739e076", null ],
    [ "puerto_salida", "hc__sr4_8c.html#aeda5c4906af2f86b41b730fd3a9ea2a5", null ]
];