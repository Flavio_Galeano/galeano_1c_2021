var searchData=
[
  ['hc_5fsr4_2ec',['hc_sr4.c',['../hc__sr4_8c.html',1,'']]],
  ['hc_5fsr4_2eh',['hc_sr4.h',['../hc__sr4_8h.html',1,'']]],
  ['hcsr04init',['HcSr04Init',['../hc__sr4_8c.html#a9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../hc__sr4_8h.html#a9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04readdistancecentimeters',['HcSr04ReadDistanceCentimeters',['../hc__sr4_8c.html#aa1b3e3a72f081db98eafa97000197a79',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c'],['../hc__sr4_8h.html#aa1b3e3a72f081db98eafa97000197a79',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c']]],
  ['hcsr04readdistanceinches',['HcSr04ReadDistanceInches',['../hc__sr4_8c.html#ac94cf11dbb224fead4118c8fbc7199c5',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c'],['../hc__sr4_8h.html#ac94cf11dbb224fead4118c8fbc7199c5',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c']]]
];
