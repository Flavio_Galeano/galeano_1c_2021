var indexSectionsWithContent =
{
  0: "bdehilmnopstu",
  1: "st",
  2: "hltu",
  3: "hlmotu",
  4: "beipt",
  5: "bm",
  6: "dltu",
  7: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "groups",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Modules",
  7: "Pages"
};

