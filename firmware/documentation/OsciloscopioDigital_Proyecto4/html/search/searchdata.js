var indexSectionsWithContent =
{
  0: "abcdefhilmnoprstuvw",
  1: "ast",
  2: "aostu",
  3: "acefhlmorstuvw",
  4: "abdefimoprstu",
  5: "s",
  6: "s",
  7: "abcdelnpru",
  8: "dstu",
  9: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Estructuras de Datos",
  2: "Archivos",
  3: "Funciones",
  4: "Variables",
  5: "Enumeraciones",
  6: "Valores de enumeraciones",
  7: "defines",
  8: "Grupos",
  9: "Páginas"
};

