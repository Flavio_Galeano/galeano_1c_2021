var searchData=
[
  ['adc0_5firqhandler',['ADC0_IRQHandler',['../analog__io_8c.html#a24969bb693bf0eb2f7e47173bddb0813',1,'analog_io.c']]],
  ['ainputs_5fburst_5fread',['AINPUTS_BURST_READ',['../analog__io_8h.html#a9439488d01a948fab1b2f8bc0bab00fa',1,'analog_io.h']]],
  ['ainputs_5fsingle_5fread',['AINPUTS_SINGLE_READ',['../analog__io_8h.html#a5b01b5f293bd90a7bf9f4a988b3fce04',1,'analog_io.h']]],
  ['alfa',['alfa',['../_osciloscopio_digital_8c.html#a5bf563dfc55db9dac98ac73ed7a8df92',1,'OsciloscopioDigital.c']]],
  ['analog_5finput_5fconfig',['analog_input_config',['../structanalog__input__config.html',1,'']]],
  ['analog_5finput_5fread',['ANALOG_INPUT_READ',['../analog__io_8h.html#a7b3118011a8b70756613058be97884ab',1,'analog_io.h']]],
  ['analog_5fio_2ec',['analog_io.c',['../analog__io_8c.html',1,'']]],
  ['analog_5fio_2eh',['analog_io.h',['../analog__io_8h.html',1,'']]],
  ['analog_5foutput_5frange',['ANALOG_OUTPUT_RANGE',['../analog__io_8h.html#a950c25493cc190f761c14db95326beee',1,'analog_io.h']]],
  ['analog_5foutput_5fwrote',['ANALOG_OUTPUT_WROTE',['../analog__io_8h.html#a731c5701b891f35c7f95766fdda7aa9b',1,'analog_io.h']]],
  ['analoginputinit',['AnalogInputInit',['../analog__io_8c.html#a75e49898ce3cf18d67e4d463c6e2a8de',1,'AnalogInputInit(analog_input_config *inputs):&#160;analog_io.c'],['../analog__io_8h.html#a75e49898ce3cf18d67e4d463c6e2a8de',1,'AnalogInputInit(analog_input_config *inputs):&#160;analog_io.c']]],
  ['analoginputread',['AnalogInputRead',['../analog__io_8c.html#ad13e6436e0177f0e17dc1a01fc4d47af',1,'AnalogInputRead(uint8_t channel, uint16_t *value):&#160;analog_io.c'],['../analog__io_8h.html#ad13e6436e0177f0e17dc1a01fc4d47af',1,'AnalogInputRead(uint8_t channel, uint16_t *value):&#160;analog_io.c']]],
  ['analoginputreadpolling',['AnalogInputReadPolling',['../analog__io_8c.html#a1673192453e03e9fab4976534b20f3e9',1,'AnalogInputReadPolling(uint8_t channel, uint16_t *value):&#160;analog_io.c'],['../analog__io_8h.html#a1673192453e03e9fab4976534b20f3e9',1,'AnalogInputReadPolling(uint8_t channel, uint16_t *value):&#160;analog_io.c']]],
  ['analogoutputinit',['AnalogOutputInit',['../analog__io_8c.html#ab57399e946247652a096a0e2d3a1b69a',1,'AnalogOutputInit(void):&#160;analog_io.c'],['../analog__io_8h.html#ab57399e946247652a096a0e2d3a1b69a',1,'AnalogOutputInit(void):&#160;analog_io.c']]],
  ['analogoutputwrite',['AnalogOutputWrite',['../analog__io_8c.html#a464364f0790a3e7d1ab9d9e9c2d9092c',1,'AnalogOutputWrite(uint16_t value):&#160;analog_io.c'],['../analog__io_8h.html#a1c53d6af3e371f4c7eaa3b7461705fc8',1,'AnalogOutputWrite(uint16_t value):&#160;analog_io.c']]],
  ['analogstartconvertion',['AnalogStartConvertion',['../analog__io_8c.html#a551f68a70593d1b97e8c41a470707368',1,'AnalogStartConvertion(void):&#160;analog_io.c'],['../analog__io_8h.html#a551f68a70593d1b97e8c41a470707368',1,'AnalogStartConvertion(void):&#160;analog_io.c']]]
];
