var searchData=
[
  ['panaloginput',['pAnalogInput',['../structanalog__input__config.html#a2298b64ab1835d56f927cb94137f36ad',1,'analog_input_config']]],
  ['period',['period',['../structtimer__config.html#a258408d6d5d13a24bfa5211d81ce1682',1,'timer_config']]],
  ['pfunc',['pFunc',['../structtimer__config.html#a9cead290357aaae808d4db4ab87784df',1,'timer_config']]],
  ['pi',['PI',['../_osciloscopio_digital_8c.html#a598a3330b3c21701223ee0ca14316eca',1,'OsciloscopioDigital.c']]],
  ['pisranaloginput',['pIsrAnalogInput',['../analog__io_8c.html#a89f6987e6cd3865c55e79ff098626afa',1,'analog_io.c']]],
  ['pisrtimera',['pIsrTimerA',['../timer_8c.html#a0cf866147f6b8097318ea162c2475649',1,'timer.c']]],
  ['pisrtimerb',['pIsrTimerB',['../timer_8c.html#ac1960b63fac3ad6ba26765021cc13ba8',1,'timer.c']]],
  ['port',['port',['../structserial__config.html#a2fa54f9024782843172506fadbee2ac8',1,'serial_config']]],
  ['pserial',['pSerial',['../structserial__config.html#a1944cd6d24e8b238d8e728d0cf201541',1,'serial_config']]],
  ['ptr_5ftec_5fgroup_5fint_5ffunc',['ptr_tec_group_int_func',['../switch_8c.html#a190e41ef86b74a4235ac9b92f64dcacf',1,'switch.c']]],
  ['ptr_5ftec_5fint_5ffunc',['ptr_tec_int_func',['../switch_8c.html#a0810f768552c94f87725b9438501a73d',1,'switch.c']]]
];
