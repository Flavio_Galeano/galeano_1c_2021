var searchData=
[
  ['tick_5ftimer_5fa',['tick_timer_a',['../timer_8c.html#a7de542841e57b39bdd064751b63444f2',1,'timer.c']]],
  ['tick_5ftimer_5fb',['tick_timer_b',['../timer_8c.html#ac265afbf68b7c53461cc59f681de3144',1,'timer.c']]],
  ['timer',['timer',['../structtimer__config.html#a0ff73b1428e9cd11a4f7a6cf46b7b550',1,'timer_config::timer()'],['../group___timer.html',1,'(Namespace global)']]],
  ['timer_2ec',['timer.c',['../timer_8c.html',1,'']]],
  ['timer_2eh',['timer.h',['../timer_8h.html',1,'']]],
  ['timer_5fa',['timer_a',['../timer_8c.html#a5eb675fb41b938841fca0dff84b88046',1,'timer_a():&#160;timer.c'],['../group___timer.html#ga47d0eac91b49052bcd964223eb8eeffc',1,'TIMER_A():&#160;timer.h']]],
  ['timer_5fa_5f1ms_5ftick',['TIMER_A_1ms_TICK',['../group___timer.html#ga162d13fe964cde50a95d80a88349be7d',1,'timer.h']]],
  ['timer_5fb',['timer_b',['../timer_8c.html#a9c95603e958951d2dab83c04154741ff',1,'timer_b():&#160;timer.c'],['../group___timer.html#ga394289ce488144cf553f2193ad4f38c3',1,'TIMER_B():&#160;timer.h']]],
  ['timer_5fb_5f1ms_5ftick',['TIMER_B_1ms_TICK',['../group___timer.html#gaef8c2dd280d8e80f0613ebf320eb2838',1,'timer.h']]],
  ['timer_5fc',['TIMER_C',['../group___timer.html#ga7f80ae7115920d2e2c6e57670471a216',1,'timer.h']]],
  ['timer_5fc_5f1us_5ftick',['TIMER_C_1us_TICK',['../group___timer.html#ga32cf8edb66a6be9668cbeaea51cfabcf',1,'timer.h']]],
  ['timer_5fconfig',['timer_config',['../structtimer__config.html',1,'']]],
  ['timera',['timerA',['../_osciloscopio_digital_8c.html#a91fd82da899fa4fdf141e724e640aed0',1,'OsciloscopioDigital.c']]],
  ['timerb',['timerB',['../_osciloscopio_digital_8c.html#aa934d986122ec4f041ce5ebba2cda893',1,'OsciloscopioDigital.c']]],
  ['timerinit',['TimerInit',['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c'],['../group___timer.html#ga148b01475111265d1798f5c204a93df0',1,'TimerInit(timer_config *timer_ini):&#160;timer.c']]],
  ['timerreset',['TimerReset',['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'TimerReset(uint8_t timer):&#160;timer.c']]],
  ['timerstart',['TimerStart',['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c'],['../group___timer.html#ga31487bffd934ce838a72f095f9231b24',1,'TimerStart(uint8_t timer):&#160;timer.c']]],
  ['timerstop',['TimerStop',['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c'],['../group___timer.html#gab652b899be3054eae4649a9063ec904b',1,'TimerStop(uint8_t timer):&#160;timer.c']]]
];
