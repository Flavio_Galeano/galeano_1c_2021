########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#####################################
#Guía 1

####Ejercicio 1
#PROYECTO_ACTIVO = Guia_1/Ejercicio_1
#NOMBRE_EJECUTABLE = Ejercicio_1.exe

####Ejercicio 7
#PROYECTO_ACTIVO = Guia_1/Ejercicio_7
#NOMBRE_EJECUTABLE = Ejercicio_7.exe

####Ejercicio 9
#PROYECTO_ACTIVO = Guia_1/Ejercicio_9
#NOMBRE_EJECUTABLE = Ejercicio_9.exe

####Ejercicio 12
#PROYECTO_ACTIVO = Guia_1/Ejercicio_12
#NOMBRE_EJECUTABLE = Ejercicio_12.exe

####Ejercicio 14
#PROYECTO_ACTIVO = Guia_1/Ejercicio_14
#NOMBRE_EJECUTABLE = Ejercicio_14.exe

####Ejercicio 16
#PROYECTO_ACTIVO = Guia_1/Ejercicio_16
#NOMBRE_EJECUTABLE = Ejercicio_16.exe

####Ejercicio 17
#PROYECTO_ACTIVO = Guia_1/Ejercicio_17
#NOMBRE_EJECUTABLE = Ejercicio_17.exe

####Integrador_A
#PROYECTO_ACTIVO = Guia_1/Integrador_A
#NOMBRE_EJECUTABLE = Integrador_A.exe

####Integrador_C
PROYECTO_ACTIVO = Guia_1/Integrador_C
NOMBRE_EJECUTABLE = Integrador_C.exe

####Integrador_D
#PROYECTO_ACTIVO = Guia_1/Integrador_D
#NOMBRE_EJECUTABLE = Integrador_D.exe