/*
 * main.c
 *
 *  Created on: 18 mar. 2021
 *      Author: flavi
 */

/* 1. Declare una constante de 32 bits, con todos los bits en 0 y el bit 6 en 1. Utilice el operador
<<. */

#include <stdio.h>

int main(void)
{
	const int32_t num = 1;
	num<<5;
	printf("%d\n", num);
	return 0;
}

/* Tira error. printf espera variable de tipo char y num es de tipo int32_t */
