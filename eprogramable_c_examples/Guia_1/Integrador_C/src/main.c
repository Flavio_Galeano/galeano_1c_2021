/*
 * main.c
 *
 *  Created on: 23 mar. 2021
 *      Author: Flavio Galeano
 */

/*Escriba una función que reciba un dato de 32 bits, la cantidad de dígitos de salida y un puntero a
un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD,
guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

int8_t BinaryToBCD (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t n=1;
	uint32_t div=data;
	uint8_t resto;
	uint8_t x;
	while(n<=digits)
	{
		resto=div%10;
		div=div/10;
		x=digits-n;
		*(bcd_number+x) = resto;
		n++;
	}
	return 0;
}

void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main (void)
{
	uint32_t data=12;
	printf("\nNúmero: %d", data);
	uint8_t digits=2;
	uint8_t bcd_number[] = {0, 0};
	uint8_t *puntero;
	puntero = &bcd_number;
	BinaryToBCD(data, digits, puntero);
	printf("\nNúmero en binario: ");
	printBits(sizeof(data), &data);
	printf("Digitos en BCD: ");
	int i=0;
	while(i<digits)
	{
		printf("%d ", bcd_number[i]);
		i++;
	}
	return 0;
}

