/*
 * main.c
 *
 *  Created on: 18 mar. 2021
 *      Author: flavi
 */

/* Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0. Si es 0,
cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa. Para la
resolución de la lógica, siga el diagrama de flujo siguiente: (img)*/

#include <stdio.h>

int main(void)
{
	int32_t num, mask=(1<<4), A=0;
	num=rand();
	mask = mask & num;
	if (mask==16)
	{
		A=170; /*en hexa aa=170*/
	}
	printf("%d\n", num);
	printf("%d\n", A);
	return 0;
}



