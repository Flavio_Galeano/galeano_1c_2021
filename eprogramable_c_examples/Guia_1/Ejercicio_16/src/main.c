/*
 * main.c
 *
 *  Created on: 22 mar. 2021
 *      Author: Flavio Galeano
 *
 *	a. Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare
cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
cargue cada uno de los bytes de la variable de 32 bits.
	b. Realice el mismo ejercicio, utilizando la definición de una “union”
 */


#include <stdio.h>
#include <stdint.h>

/*a*/

/*
#define unos 255

int main(void)
{
	uint32_t var=0x01020304;
	uint8_t a=unos;
	uint8_t b=unos;
	uint8_t c=unos;
	uint8_t d=unos;
	a=unos & (var>>24);
	b=unos & (var>>16);
	c=unos & (var>>8);
	d=unos & var;

	printf("%d", var);
	printf("\n\n%d", a);
	printf("\n%d", b);
	printf("\n%d", c);
	printf("\n%d", d);

	return 0;
}
*/
/*b*/

int main(void)
{
	uint32_t var=0x01020304;

	union test
	{
		struct cada_byte
		{
				uint8_t byte1;
				uint8_t byte2;
				uint8_t byte3;
				uint8_t byte4;
		}cada_byte;
	uint32_t todos_los_bytes;
	}union_1;

	printf("%d", var);

	union_1.todos_los_bytes=var;

	printf("\n\n%d", union_1.cada_byte.byte1);
	printf("\n%d", union_1.cada_byte.byte2);
	printf("\n%d", union_1.cada_byte.byte3);
	printf("\n%d", union_1.cada_byte.byte4);

	return 0;
}

