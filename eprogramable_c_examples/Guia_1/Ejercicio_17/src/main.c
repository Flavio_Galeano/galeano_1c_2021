/*
 * main.c
 *
 *  Created on: 22 mar. 2021
 *      Author: Flavio Galeano
 *
 *Realice un programa que calcule el promedio de los 15 números listados abajo, para ello,
primero realice un diagrama de flujo similar al presentado en el ejercicio 9. (Puede utilizar la
aplicación Draw.io). Para la implementación, utilice el menor tamaño de datos posible:
234 123 111 101 32
116 211 24 214 100
124 222 1 129 9
 */

/*Posible camino para ocupar menos memoria:
 * Hacer promedio número por número. Es decir a/15+b/15+c/15... para no sumar
 * todo primero y que quede un número de 16 bits. Van a ir dando decimales así
 * habrá que redondear. Esto puede derivar en un error (grande o chico)*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#define TOTAL 15

int main(void)
{
	uint8_t num[total]={234, 123, 111, 101, 32, 116, 211, 24, 214, 100, 124, 222, 1, 129, 9};
	uint16_t suma=0;
	uint8_t i=0;

	while(i<sizeof(num))
	{
		suma=suma+num[i];
		i++;
	}

	suma=suma/TOTAL;
	printf("Promedio: %d", suma);

	return 0;
}

