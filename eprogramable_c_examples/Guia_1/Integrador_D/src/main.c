/*
 * main.c
 *
 *  Created on: 23 mar. 2021
 *      Author: Flavio Galeano
 */

/*Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
gpioConf_t.
typedef struct
{
	/*uint8_t port; !< GPIO port number */
	/*uint8_t pin; !< GPIO pin number */
	/*uint8_t dir; !< GPIO direction ‘0’ IN; ‘1’ OUT */
/*} gpioConf_t;
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14
[image]
La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector
anterior operar sobre el puerto y pin que corresponda.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

int8_t BinaryToBCD (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t n=1;
	uint32_t div=data;
	uint8_t resto;
	uint8_t x;
	while(n<=digits)
	{
		resto=div%10;
		div=div/10;
		x=digits-n;
		*(bcd_number+x) = resto;
		n++;
	}
	return 0;
}


typedef struct
{
	uint8_t port; /*!< GPIO port number */
	uint8_t pin; /*!< GPIO pin number */
	uint8_t dir; /*!< GPIO direction ‘0’ IN; ‘1’ OUT */
} gpioConf_t;

void BcdToBits(uint32_t num, gpioConf_t *gpio)
{
	/*Se inician todos los bits en 0**/
	uint8_t n[4]={0,0,0,0};
	uint8_t i=0;

	if((num==8) | (num==9))
	{
		n[0]=1;
	}
	if((num==4) | (num==5) | (num==6) | (num==7))
	{
		n[1]=1;
	}
	if((num==2) | (num==3) | (num==6) | (num==7))
	{
		n[2]=1;

	}
	if((num==1) | (num==3) | (num==5) | (num==7) | (num==9))
	{
		n[3]=1;
	}

	while(i<4)
	{
		printf("\nEl bit del puerto %d", (gpio+i)->port);
		printf(".%d", (gpio+i)->pin);
		printf(" es: %d", n[i]);
		i++;
	}
	printf("\n \n");
	i=0;
	while(i<4)
	{
		printf("%d", n[i]);
		i++;
	}
}

int main(void)
{
	uint32_t num;
	gpioConf_t b[4];

	/*Se definen los ports, pins y dir*/
	b[0].port=1;
	b[1].port=1;
	b[2].port=1;
	b[3].port=2;
	b[0].pin=4;
	b[1].pin=5;
	b[2].pin=6;
	b[3].pin=14;
	b[0].dir=1;
	b[1].dir=1;
	b[2].dir=1;
	b[3].dir=1;

	/*Ingresar el número de 0 a 9*/
	num=9;

	BcdToBits(num, &b);

	return 0;
}
