/*
 * main.c
 *
 *  Created on: 21 mar. 2021
 *      Author: Flavio Galeano
 * Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
caracteres y edad.
a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
b. Defina un puntero a esa estructura y cargue los campos con los datos de su
compañero (usando acceso por punteros). */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

int main(void)
{
	struct alumno
	{
		char nombre[12];
		char apellido[20];
		uint8_t edad;
	}
	A, *p_alumno;
	strcpy(A.nombre, "Flavio");
	strcpy(A.apellido, "Galeano");
	A.edad=24;
	printf("\nMi nombre: %s ", A.nombre);
	printf("%s", A.apellido);           /*##Para probar si entraron bien los datos*/

	/*Datos del compañero*/
	p_alumno=&A;
	char nombre_companero[12];
	char apellido_companero[20];
	printf("\nNombre del compañero: ");
	scanf("%s", nombre_companero);
	printf("\nApellido del compañero: ");
	scanf("%s", apellido_companero);
	/*printf("%s", nombre_companero);
	printf(" %s", apellido_companero);   /*##Para probar si entraron bien los datos*/

	strcpy(p_alumno->nombre, nombre_companero);
	strcpy(p_alumno->apellido, apellido_companero);

/*Dos errores: Uno que toma los scanf al principio de la ejecución y no desde su lugar
 * en el código.
 * Segundo que los strcpy causan un "stackdump"*/

	/*Probando funcionamiento*/
	printf("\nSalida: %s ", A.nombre);
	printf("%s", A.apellido);
	return 0;
}




