/*
 * main.c
 *
 *  Created on: 23 mar. 2021
 *      Author: Flavio Galeano
 */

/*Realice un función que reciba un puntero a una estructura LED como la que se muestra a
continuación:
struct leds
{
uint8_t n_led; indica el número de led a controlar
uint8_t n_ciclos; indica la cantidad de ciclos de encendido/apagado
uint8_t periodo; indica el tiempo de cada ciclo
uint8_t mode; ON, OFF, TOGGLE
} my_leds;
Use como guía para la implementación el siguiente diagrama de flujo: [image]*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

typedef struct leds
	{
		uint8_t n_led; /*indica el número de led a controlar*/
		uint8_t n_ciclos; /*indica la cantidad de ciclos de encendido/apagado*/
		uint8_t periodo; /*indica el tiempo de cada ciclo*/
		uint8_t mode; /*1=ON, 0=OFF, 2=TOGGLE*/
	} my_leds_t;
	my_leds_t my_leds;


void activar_leds(my_leds_t *dir);

int main(void)
{
	/*Acá se definen los parametros que se le pasan a la función*/
	my_leds.n_led=2;
	my_leds.n_ciclos=3;
	my_leds.periodo=5;
	my_leds.mode=2;

	activar_leds(&my_leds);

	return 0;
}


void activar_leds(my_leds_t *dir)
{
	uint8_t i=0;
	switch(dir->mode)
	{
		/*Encendido*/
		case 1 :
			printf("Led %d encendido", dir->n_led);
		break;
		/*Apagado*/
		case 0 :
			printf("Led %d apagado", dir->n_led);
		break;
		/*Toggle*/
		case 2 :
			while(i<(dir->n_ciclos))
			{
					switch(dir->n_led)
					{
						case 0 :
							if (i==0)
							{
								printf("Led %d: ", dir->n_led);
								printf("encendido... (%d segundos)... apagado... ", dir->periodo);
							}
							else
							{
								printf("(%d segundos)... encendido... ", dir->periodo);
								printf("(%d segundos)... apagado... ", dir->periodo);
							}
						break;
						case 1 :
							if (i==0)
							{
								printf("Led %d: ", dir->n_led);
								printf("encendido... (%d segundos)... apagado... ", dir->periodo);
							}
							else
							{
								printf("(%d segundos)... encendido... ", dir->periodo);
								printf("(%d segundos)... apagado... ", dir->periodo);
							}
						break;
						case 2 :
							if (i==0)
							{
								printf("Led %d: ", dir->n_led);
								printf("encendido... (%d segundos)... apagado... ", dir->periodo);
							}
							else
							{
								printf("(%d segundos)... encendido... ", dir->periodo);
								printf("(%d segundos)... apagado... ", dir->periodo);
							}
						break;
					}
					i++;
			}
	}
}

