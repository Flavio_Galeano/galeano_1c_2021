/*
 * main.c
 *
 *  Created on: 21 mar. 2021
 *      Author: Flavio Galeano
 */
/* 12. Declare un puntero a un entero con signo de 16 bits y cargue inicialmente el valor -1. Luego,
mediante máscaras, coloque un 0 en el bit 4.
<<. */

#include <stdio.h>
#include <stdint.h>

void printBits(size_t const size, void const * const ptr) //Uso esta funcion para representar en binario
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}

int main(void)
{
	int16_t num=-1;
	int16_t mask=~(1<<4);
	int16_t *point;
	point = &num;

	printBits(sizeof(num), point); /*Para ver el número antes de cambiarlo*/
	printf("\n");
	*point= (num & mask);
	printBits(sizeof(num), point); /*Número cambiado*/

	return 0;
}


