/*
 * main.c
 *
 *  Created on: 18 mar. 2021
 *      Author: flavi
 */

/* 7.Sobre una variable de 32 bits sin signo previamente declarada y de valor desconocido,
asegúrese de colocar el bit 3 a 1 y los bits 13 y 14 a 0 mediante máscaras y el operador <<.*/

#include <stdio.h>

#define A 4096
#define B 6

int main(void)
{
	uint32_t num;
	num=rand();
	printf("%d\n", num);
	/*se deben usar como máscaras los números
	* 4096 (0001000000000000) con un OR y
	*    6 (0000000000000110) con un AND
	* luego correrlos un bit para atrás*/
	num = num | A;
	num = num & B;
	num << 1;
	printf(" - ");
	printf("%d\n", num);
	return 0;
}



